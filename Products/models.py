from django.db import models
from django.shortcuts import render
from wagtail.core.fields import RichTextField
from wagtail.core.models import Orderable, Page
from django import forms
from wagtail.snippets.models import register_snippet
from wagtail.contrib.routable_page.models import RoutablePageMixin, route
from taggit.models import TaggedItemBase, Tag as TaggitTag
from wagtail.admin.edit_handlers import (
    FieldPanel,
    InlinePanel,
    PageChooserPanel,
)
from modelcluster.tags import ClusterTaggableManager
from wagtail.images.edit_handlers import ImageChooserPanel

from modelcluster.fields import (
    ParentalKey,
    ParentalManyToManyField
)
class ProductIndexPage(RoutablePageMixin,Page):
    intro = RichTextField(blank=True)
    content_panels = Page.content_panels + [
        FieldPanel("intro"),
    ]
    subpage_types = [
        "Products.Product",
    ]
    max_count = 1


    def get_posts(self):
        return ProductIndexPage.objects.descendant_of(self).live()

    def get_context(self, request, *args, **kwargs):
        context = super().get_context(request)
        context["products"] = Product.objects.all()
        context['categories'] = ProductCategory.objects.all()
        context['i'] = 0
        print(context['products'])
        return context


    @route(r'^category/(?P<category>[-\w]+)/$')
    def post_by_category(self, request, category, *args, **kwargs):
        context = super().get_context(request)
        self.search_type = 'category'
        self.search_term = category
        context['categories'] = ProductCategory.objects.all()
        context["products"] = Product.objects.all().filter(Categories__slug = category)
        return render(request, 'Products/product_list.html',context )

    @route(r'^search/$')
    def post_search(self, request, *args, **kwargs):
        search_query = request.GET.get('q', None)
        context = super().get_context(request)
        context['categories'] = ProductCategory.objects.all()
        self.posts = self.get_posts()
        print(self.posts)
        if search_query:
            context['products'] = self.posts.filter(title__contains=search_query)
            self.search_term = search_query
            self.search_type = 'search'
        return render(request, 'Products/product_list.html',context )



class Product(Page):
    date = models.DateTimeField(blank=True, null=True)
    Description = models.TextField(blank = True)
    Price = models.DecimalField(max_digits=10, decimal_places=2)
    Available = models.BooleanField(default=True)
    TopFeatured = models.BooleanField(default=False)
    Categories=ParentalManyToManyField("Products.ProductCategory", blank=True)
    tags = ClusterTaggableManager(through ="Products.ProductTag", blank=True)
    sex = models.CharField(max_length =20,choices = (("1", 'Male'), ('2' , 'Female'), ('3', 'Unisex'), ('4', 'Kids')) , default = '1')
    subpage_types = []
    content_panels = Page.content_panels + [
        FieldPanel("Description", classname="full"),
        FieldPanel("Price"),
        FieldPanel("Available"),
        FieldPanel("TopFeatured"),
        FieldPanel('tags'),
        FieldPanel('sex'),
        FieldPanel('Categories', widget =forms.CheckboxSelectMultiple),
        InlinePanel('gallery_images', label="Gallery images"),
    ]


    @property
    def get_product_index_page(self):
        return self.get_parent().specific

    def get_context(self, request, *args, **kwargs):
        context = super().get_context(request)
        context["product_index_page"] = self.get_product_index_page
        context["products"] = Product.objects.all()
        return context


class ProductGalleryImage(Orderable):
    page = ParentalKey(Product, related_name='gallery_images')
    image = models.ForeignKey('wagtailimages.Image', on_delete=models.CASCADE, related_name='+')
    caption = models.CharField(blank=True, max_length=250)
    panels = [ImageChooserPanel('image'),FieldPanel('caption'),]

@register_snippet
class ProductCategory(models.Model):
    name= models.CharField(max_length=255)
    slug=models.SlugField(unique=True,max_length=80)
    Caterogy_Description = models.TextField(blank=True)
    image = models.ForeignKey(
        "wagtailimages.Image", on_delete=models.SET_NULL, null=True, related_name="+"
    )
    panels=[
        FieldPanel('Caterogy_Description'),
        FieldPanel('name'),
        FieldPanel('slug'),
        ImageChooserPanel('image')
    ]
    def __str__(self):
        return self.name

    def get_all_category(self):
        all_category=ProductCategory.Objects.all()
        return all_category
        
    class Meta:
        verbose_name = 'Category'
        verbose_name_plural='Categories'

class ProductTag(TaggedItemBase):
    content_object=ParentalKey('Product',related_name='product_tag')

@register_snippet
class Tag(TaggitTag):
    class Meta:
        proxy=True